package lists;

import interfaces.EList;
import interfaces.Map;

import java.awt.List;
import java.util.Comparator;

import comparators.KeyComparator;

import support.MapEntry;

public class SCDLL_Map<K, V> implements Map<K,V>{ //Basically Positional List Logic but non sequential.  Map Ordered by Keys.
	private Comparator<MapEntry<K,V>> cmp;
	private SortedCircularDoublyLinkedList<MapEntry<K,V>> list;
	
	
	
	public SCDLL_Map() {
		cmp = new KeyComparator();
		list = new SortedCircularDoublyLinkedList<MapEntry<K,V>>(cmp);
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		return list.size()==0;
	}

	@Override
	public V get(Object key) {
		for(int i =0; i<this.list.size(); i++) {
			if(this.list.get(i).getKey().equals(key)) return this.list.get(i).getValue();
		}
		return null;
	}

	@Override
	public V put(Object key, Object value) { //Inserts a Value with a specific key. If there is already an entry with that key, it replaces the value and returns the previous value. 
		MapEntry<K,V> temp = new MapEntry((K)key,(V)value);
		V res = null;
		if(this.get(key)!=null) res = this.remove(key);
		this.list.add(temp);
		return res;
	}

	@Override
	public V remove(Object key) { //Removes the Map Entry with the given key value and returns the value it held.
		V temp = this.get(key);
		for(int i=0; i<this.size(); i++) {
			if(this.list.get(i).getKey().equals(key)) this.list.remove(this.list.get(i));
		}
		return temp;
	}

	@Override
	public boolean contains(Object key) {
		return this.get(key)!=null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EList<K> getKeys() {
		EList<K> l =  new SinglyLinkedList<K>();
		for(int i = 0; i<this.list.size(); i++) {
			l.add((K)this.list.get(i).getKey());
		}
		return l;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public EList<V> getValues() {
		EList<V> l =  new SinglyLinkedList<V>();
		for(int i = 0; i<this.list.size(); i++) {
			l.add((V)this.list.get(i).getValue());
		}
		return l;
	}

	@Override
	public void makeEmpty() {
		this.list.clear();
	}

}
