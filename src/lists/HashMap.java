package lists;

import interfaces.EList;
import interfaces.Map;
import support.MapEntry;

//Hash function: h(key) = hashCode(key) % N, where N is an Array length.

/*Integer i : (i >> 32) + i, Take number i, shift it 32 bits right and add i.

Strings s : (e.g., “Apu”) Simple:
		int hashCode = 0;
		for (int i=0; i < s.length();++i){
			hashCode += s.charAt(i);
		}	
		return hashCode;
Add all characters in the string*/


public class HashMap<K,V> implements Map<K,V>{
	
	private int size;
	private SinglyLinkedList<MapEntry<K,V>>[] bucket;
	private static final int DEFAULT_SIZE = 10;
	
	private int hashFunction(K key) {
		return key.hashCode() % this.bucket.length;
	}
	
	public HashMap(int num) {
		size= 0;
		bucket = new SinglyLinkedList[num];
		for(int i = 0; i<= bucket.length; i++) {
			bucket[i] = new SinglyLinkedList<MapEntry<K,V>>();
		}
		
	}
	
	public HashMap(){
		this(DEFAULT_SIZE);
	}

	
	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public V get(K key) {
		SinglyLinkedList<MapEntry<K,V>> l = bucket[this.hashFunction(key)];
		for(int i=0; i<l.size(); i++) {
			if(l.get(i).getKey().equals(key)) return l.get(i).getValue();
		}
		
		return null;
	}

	@Override
	public V put(K key, V value) { //Adds and replaces a value in a certain list in a bucket;
		V res = this.get(key);
		if(res!=null) { this.remove(key);}
		SinglyLinkedList<MapEntry<K,V>> l = bucket[this.hashFunction(key)];
		l.add(new MapEntry(key, value));
		this.size++;
		return res;
	}

	@Override
	public V remove(K key) {
		if(this.isEmpty()) return null;
		V res = this.get(key);
		if(res!= null) {
			SinglyLinkedList<MapEntry<K,V>> l = bucket[this.hashFunction(key)];
			for(int i =0; i<l.size(); i++) {
				if(l.get(i).getKey().equals(key)) l.remove(i);
			} 
			this.size--;
		}
		return res;
	}

	@Override
	public boolean contains(K key) {
		return !this.get(key).equals(null);
	}

	@Override
	public void makeEmpty() {
		for(int i=0; i<this.size; i++) {
			this.bucket[i].clear();
		}
		this.size =0;
	}

	@Override
	public EList<K> getKeys() { //O(n^2). One must die, for the rest to live!
		EList<K> list = new SinglyLinkedList<K>();
		for(int i=0; i<this.size(); i++) {
			for(int j =0; j<bucket[i].size(); i++){
				list.add(bucket[i].get(j).getKey());
		}
	}
		return list;
	}

	@Override
	public EList<V> getValues() { //O(n)
		EList<V> l = new SinglyLinkedList<V>();
		EList<K> temp = this.getKeys();
		for(int i=0; i<temp.size(); i++) {
			l.add(this.get(temp.get(i)));
		}
		return l;
	}

	

}
