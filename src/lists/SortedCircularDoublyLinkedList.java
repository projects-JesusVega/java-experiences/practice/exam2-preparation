package lists;


import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

import java.util.ListIterator;

import interfaces.CList;
import support.DNode;

public class SortedCircularDoublyLinkedList<E> implements CList<E>{
	private int size;
	private DNode<E> header=null;
	private Comparator<E> cmp; //Comparator done in generics; not applied specifically for car objects

	
	
	public SortedCircularDoublyLinkedList(Comparator<E> cmp) {
		this.header = new DNode(null, null, null); //DNode structure (element, previous node, next node);
		this.size = 0;
		this.cmp = cmp;
	}
	
	
	@Override
	public Iterator iterator() { //Provides an Iterator object for this list. It creates an ArrayList and utilizes the iterator of that ArrayLis . Progress: Complete
		List<E> arrl = new ArrayList<E>();
		DNode<E> nde = header.getNext();
		while(nde!=header) { arrl.add(nde.getElement()); nde=nde.getNext();}
		return arrl.iterator(); //Test2
	}

	@Override
	public boolean add(Object obj) {
		DNode<E> firstnde = new DNode<E>((E)obj);
		if(this.isEmpty()) {
			header.setNext(firstnde);
			header.setPrev(firstnde);
			firstnde.setNext(header);
			firstnde.setPrev(header);
			size++;
			return true;
		}
		else{
			DNode<E> nde = header.getNext();
			while(nde != header){
				if(this.cmp.compare((E)obj, nde.getElement())<0) {
					nodeBefore(nde, firstnde);
					return true;
				}
				if(nde.getNext() == header) {
					nodeBefore(header, firstnde);
					return true;
				}
				nde = nde.getNext();
			}return false;}
	}
	
	private void nodeBefore(DNode<E> ndeA, DNode<E> nde) { //Helper method primarily for the add method.
		DNode<E> ndeB = ndeA.getPrev(); 
		ndeB.setNext(nde);
		ndeA.setPrev(nde);
		nde.setNext(ndeA);
		nde.setPrev(ndeB);
		size++; 
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(Object obj) {/* This remove method takes an object as a parameter; It iterates through the list and removes the node which contains the given element, if any.
		 It removes the node pointers towards that node and cleans the memory location*/
		if(isEmpty()) return false; 
		if(this.contains(obj)){
			DNode<E> nde = header.getNext();
			while(nde!=header){
				if(nde.getElement().equals(obj)){ //TODO check
					nde.getPrev().setNext(nde.getNext());
					nde.getNext().setPrev(nde.getPrev());
					nde.cleanLinks();
					size--;
					return true;
				}
				nde=nde.getNext();
			}
			
		}
		return false;

	}
	@Override
	public boolean remove(int index) { /*This remove method is based on a given index; it utilizes the basic remove method by utilizing an object
	from the get(int i) method as a parameter; the get searches the list for any posible object located at "index"	*/
		if(index<0 || index>this.size()-1) throw new IndexOutOfBoundsException("This index is not valid for this list");
		return this.remove(this.get(index)); //Complexity is 2n, which means is O(n), where n is the size of the list.
	
	}

	@Override
	public int removeAll(Object obj) { /* The removeAll method receives and object and it executes a while loop. As long as the
	object is present on the lit, the method will call upon the remove(Object e) method in order to remove it. Each time an object is removed */
		int count = 0;
		while(this.contains(obj)) {
			this.remove(obj);
			count++;
		}
		return count;
	}

	@Override
	public E first() { // The method first() returns the first element on the list. TODO Ask Lucia if the exception is preferred.
		if(this.isEmpty()) return null;
		else return header.getNext().getElement();
	}

	@Override
	public E last() { // The method first() returns the first element on the list. The method returns null if the list is empty.
		if(this.isEmpty()) return null;
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) { //
		if(index<0 || index>this.size()-1) throw new IndexOutOfBoundsException("This index is not valid for this list");
		if(!this.isEmpty()){
		DNode<E> nde = header.getNext();
		int count=0;
		while(count<index) {
			nde = nde.getNext();
			count++;
		}return nde.getElement();
		} else return null;
	}

	@Override
	public void clear() { /*The clear method verifies if the list is not empty; while its not empty, it will continuously keep removing
	the first element until its empty.*/
	while(!this.isEmpty()){
		this.remove(0);
	}	
	}

	@Override
	public boolean contains(Object e) { /* The method contains(object e) iterates through the list verifying each element in each node.
	if the element is equal to the given element, then the method will return true, therwise it'll return false.*/
		if(isEmpty()) return false;
		DNode<E> nde = header.getNext();
		while(nde!=header){
			if(nde.getElement().equals(e))
				return true;
			nde= nde.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public int firstIndex(Object e) { /*The method firstIndex checks if the given object is present on the list and returns 
		the exact position in which the first instance of that object is located. */
		if(this.contains(e)){ 
			DNode<E> nde = header.getNext();
			int count=0;
			while(!nde.getElement().equals(e)) {
			count++;
			nde = nde.getNext();
			}
			return count;
		}
	return -1;
	}

	@Override
	public int lastIndex(Object e) { /*The method lastIndex checks if the given object is present on the list and returns 
		the exact position in which the last instance of that object is located. */
		if(this.contains(e)) {
		DNode<E> nde = header.getPrev();
		int count=this.size()-1;
		while(!nde.getElement().equals(e)) {
			count--;
			nde = nde.getPrev();
		}
		return count;
		}
		return -1;
	}


	
}
