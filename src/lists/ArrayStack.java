package lists;

import java.io.PrintStream;

import interfaces.Stack;

public class ArrayStack<E> implements Stack<E>{ //Last In First Out Structure
	private E[] elements;
	private int size;
	private final int DEFAULT_SIZE = 10;
	
	public ArrayStack() {
	 elements = (E[]) new Object[DEFAULT_SIZE];
	 size = 0;
	}

	@Override
	public int size() { return this.size;}

	@Override
	public boolean isEmpty() { return this.size() ==0;}

	@Override
	public E top() {
		if(this.isEmpty()) return null;
		else { return this.elements[size-1];}
	}

	@Override
	public E pop() {
		E temp = this.elements[this.size - 1];
		this.elements[this.size-1] = null;
		this.size--;
		return temp;
	}

	@Override
	public void push(Object e) {
	if(this.size == this.DEFAULT_SIZE) { reAcomodate();}
	this.elements[this.size++] = (E)e;	
	}
	
	private void reAcomodate() {
		E[] arr = (E[]) new Object[DEFAULT_SIZE*2];
		for(int i=0; i<this.size; i++) {
			arr[i]= elements[i];
		}
		elements = arr;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) { this.pop();}
	}

	@Override
	public void print(PrintStream out) {
		for (int i=this.size()-1; i>=0; --i) {
			out.println(this.elements[i]);
		}
	}

}
