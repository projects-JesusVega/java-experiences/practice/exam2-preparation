package lists;

import java.io.PrintStream;

import interfaces.Queue;

public class ArrayQueue<E> implements Queue<E> {
	private E[] elements;
	private int size;
	private final int DEFAULT_SIZE = 10;
	
	public ArrayQueue() {
		elements = (E[]) new Object[DEFAULT_SIZE];
		size = 0;
	}
	
	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public E front() {
		if(this.isEmpty()) return null;
		return elements[0];
	}

	@Override
	public E dequeue() {
		if(!this.isEmpty()) {
		E temp = elements[0];
		for(int i=0; i<this.size()-1; i++) {
			elements[i] = elements[i+1];
		}
		elements[this.size--]=null;
		return temp;
		}
		return null;
	}

	@Override
	public void enqueue(Object e) {
		if(this.size==this.DEFAULT_SIZE) reAcomodate();
		elements[this.size++] = (E) e;
	}
	
	private void reAcomodate() {
		E[] arr = (E[]) new Object[DEFAULT_SIZE*2];
		for(int i=0; i<this.size; i++) {
			arr[i]= elements[i];
		}
		elements = arr;
	}

	@Override
	public void makeEmpty() {
		while(!this.isEmpty()) this.dequeue();
	}

	@Override
	public void print(PrintStream P) {
		for (int i=this.size()-1; i>=0; --i) {
			System.out.println(this.elements[i]);
		}
		
	}

	public void addQueue(ArrayQueue<E> q) { //This method add all the elements of a given ArrayQueue to the current one.
		for(int i = 0; i<q.size(); i++) {
			this.enqueue(q.elements[i]);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void insertAfterEach (E obj1, E obj2){
		SinglyLinkedList<E> list = new SinglyLinkedList<E>();
		for(int i = 0; i<this.size(); i++) {
			if(this.elements[i].equals(obj1)) {list.add(elements[i]); list.add(obj2);}
			else { list.add(this.elements[i]);}
		}
		elements = (E[]) list.toArray();
	}
}
