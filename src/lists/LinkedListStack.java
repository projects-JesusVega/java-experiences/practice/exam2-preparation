package lists;

import java.io.PrintStream;

import interfaces.Stack;
import support.DNode;

public class LinkedListStack<E> implements Stack{ //Implementation with Double referenced Node.
	private DNode<E> header;
	private DNode<E> tail;
	private int size;
	
	public LinkedListStack() { //Is done by dummy header logic.
		header = null;
		tail = null;
		size = 0;
	}
	
	@Override
	public int size() {return this.size;}

	@Override
	public boolean isEmpty() {return this.size == 0;}

	/*@Override
	public Object top() { 
		if(this.isEmpty()) return null;
		DNode<E> nde = header;
		while(nde.getNext()!= tail) {
			nde = nde.getNext();
		}
		return nde.getElement();
	}*/
	
	@Override
	public Object top() { //One line version
		if(this.isEmpty()) return null;
		return tail.getPrev().getElement();
	}
	
	
	@Override
	public Object pop() {
		if(this.isEmpty()) return null;
		E temp = tail.getPrev().getElement();
		DNode<E> nde = tail.getPrev();
		nde.getPrev().setNext(tail);
		tail.setPrev(nde.getPrev());
		nde.cleanLinks();
		this.size--;
		return temp;
		
	}


	@Override
	public void push(Object e) {
		if(this.isEmpty()){new DNode((E)e,header, tail); this.size++;}
		DNode<E> temp = new DNode<E>((E)e, tail.getPrev(), tail);
		tail.getPrev().setNext(temp);
		tail.setPrev(temp);
		this.size++;
		
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) this.pop();
		
	}

	@Override
	public void print(PrintStream out) {
		DNode<E> nde = tail;
		while(nde != header)
			out.println(nde.getPrev().getElement());
			nde = nde.getPrev();
		}
}
