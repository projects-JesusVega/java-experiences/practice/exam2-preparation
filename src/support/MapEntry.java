package support;

import java.security.InvalidParameterException;

public class MapEntry<K,V> {
	K key;
	V value;
	
	public MapEntry(K key, V value) {
		super();
		if(key.equals(null) || value.equals(null)) throw new InvalidParameterException();
		this.key = key;
		this.value = value;
	}
	public K getKey() {
		return key;
	}
	public void setKey(K key) {
		this.key = key;
	}
	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}
	
	
	

}
