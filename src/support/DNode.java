package support;

import interfaces.Node;

public class DNode<E> implements Node<E> {
	private E element; 
	private DNode<E> prev, next; 

	// Constructors
	public DNode() {}
	
	public DNode(E e) { 
		element = e; 
	}
	
	public DNode(E e, DNode<E> p, DNode<E> n) { 
		prev = p; 
		next = n; 
	}
	
	// Methods
	public DNode<E> getPrev() {
		return prev;
	}
	public void setPrev(DNode<E> prev) {
		this.prev = prev;
	}
	public DNode<E> getNext() {
		return next;
	}
	public void setNext(DNode<E> next) {
		this.next = next;
	}
	public E getElement() {
		return element; 
	}

	public void setElement(E data) {
		element = data; 
	} 
	
	/**
	 * Just set references prev and next to null. Disconnect the node
	 * from the linked list.... 
	 */
	public void cleanLinks() { 
		prev = next = null; 
		element = null;
	}
	
}




