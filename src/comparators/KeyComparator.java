package comparators;

import java.util.Comparator;
import support.MapEntry;

public class KeyComparator<K,V> implements Comparator<MapEntry<K,V>>{

	@Override
	public int compare(MapEntry<K,V> o1, MapEntry<K,V> o2) {
		if(o1.equals(null)) return 1;
		if(o2.equals(null)) return 1;
		return o1.getKey().toString().compareTo(o2.getKey().toString());
	}


}
