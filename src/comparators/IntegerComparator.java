package comparators;

import java.util.Comparator;

public class IntegerComparator implements Comparator<Integer> {
	
	public int compare(Integer o1, Integer o2) { 
		if(o1.equals(null) || o2.equals(null)) throw new IllegalArgumentException("Input not a number");
		return o1.compareTo(o2);
	}


}
