package comparators;

import java.util.Comparator;

public class StringComparator implements Comparator<String> {
	
	@Override
	public int compare(String o1, String o2) {
		if(o1.equals(null)) return 1;
		if(o2.equals(null)) return 1;
		return o1.compareTo(o2.toString());
	}

}
