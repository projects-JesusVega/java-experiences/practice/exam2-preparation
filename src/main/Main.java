package main;

import java.util.Queue;
import java.util.Stack;

import comparators.StringComparator;
import interfaces.EList;
import lists.ArrayQueue;
import lists.ArrayStack;
import lists.LinkedListStack;
import lists.SCDLL_Map;
import lists.SinglyLinkedList;
import lists.SortedCircularDoublyLinkedList;

public class Main<E, K, V> {
	
	public static void main(String[] args) {
		
	}
	
	public Stack<E> copyStack(LinkedListStack<E> S){ //Copy the contents of a Stack onto another without altering the original Stack.
		LinkedListStack<E> temp = new LinkedListStack<E>();
		Stack<E> result = new Stack<E>();
		E element;
		while(!S.isEmpty()) {
			temp.push(S.pop());
		}
		while(!temp.isEmpty()) {
			element = (E) temp.pop();
			result.push(element);
			S.push(element); //Adds it back to the original List;
		}
		return result; //Try
	}

	public void deleteFromStack(Stack<E> stack, E obj) { //Delete from stack all copies of an elements without affecting the relative order of the stack
		Stack<E> temp = new Stack<E>();
		while(!stack.isEmpty()) {
			E t = stack.pop();
			if(!t.equals(obj))temp.push(t);	
		}
		while(!temp.isEmpty()) stack.push(temp.pop());	
	}
	
	public void deleteFromQueue(ArrayQueue<E> q, E obj){ //Delete from queue all copies of an elements without affecting the relative order of the queue
		ArrayQueue<E> temp = new ArrayQueue<E>();
		while(!q.isEmpty()) {
			E e = q.dequeue();
			if(!e.equals(obj)) temp.enqueue(e);
		}
		while(!temp.isEmpty())q.enqueue(temp.dequeue());	
	}
	
	public ArrayQueue<E> copyQueue(ArrayQueue<E> src){//Return copy of a queue. O(n) complexity.
		ArrayQueue<E> temp = new ArrayQueue();
		ArrayQueue<E> res = new ArrayQueue();
		while(!src.isEmpty()) {
			temp.enqueue(src.dequeue());
		}
		while(!temp.isEmpty()) {
			E el = temp.dequeue();
			src.enqueue(el);
			res.enqueue(el);
		}
		return res;
	}
	
    public static <E> Queue<E> beforeInQueue(ArrayQueue<E> q, E e){
    	SinglyLinkedList<E> list = new SinglyLinkedList<E>();
    	ArrayQueue<E> res = new ArrayQueue<E>();
    	while(!q.isEmpty()) {
    		list.add(q.dequeue());
    	}
    	if(!list.contains(e)) return (Queue<E>)res;
    	for(int i = 0; i<list.firstIndex(e); i++) {
    		res.enqueue(list.get(i));
    	}
    	for(int i=0; i<list.size(); i++) {
    		q.enqueue(list.get(i));
    	}
    	list.clear();
    	return (Queue<E>) res;
    	
    }

    
    
    
    
	public static boolean isSorted(SinglyLinkedList<String> L) {
		for(int i=0; i<L.size()-1; i++) {
			if(L.get(i).compareTo(L.get(i+1))>0) return false;
		}
		return true;
	}
	
	public static void removeDuplicates(SinglyLinkedList L){
		SinglyLinkedList l = new SinglyLinkedList();
		for(int i=0; i<L.size(); i++){
			if(!l.contains(L.get(i))) l.add(L.get(i));
		}
		L.clear();
		L = l;
	}

	public String removeMinQueue(ArrayQueue<String> Q){ //Removes the shortest string in a Queue of Strings. Complexity O(n)
		String str = Q.front(), ins;
		ArrayQueue<String> temp = new ArrayQueue();
		while(!Q.isEmpty()) { //O(n)
			ins = Q.dequeue(); 
			if(str.compareTo(ins)<0) str = ins;
			temp.enqueue(ins);
		}
		while(!temp.isEmpty()) { // O(n)
			ins= temp.dequeue();
			if(!ins.equals(str)) {Q.enqueue(ins);}
		}
		return str;
	}

	public Queue<String> stacktoSortedQueue(ArrayStack<String> S){ //Returns Sorted Queue without changing the original Stack.
		SortedCircularDoublyLinkedList<String> list = new SortedCircularDoublyLinkedList(new StringComparator());
		ArrayStack<String> arr = new ArrayStack<String>();
		ArrayQueue<String> result = new ArrayQueue<String>();
		while(!S.isEmpty()) {
			arr.push(S.pop());
		}
		while(!arr.isEmpty()) {
			String temp = arr.pop();
			list.add(temp);
			S.push(temp);
		}
		for(int i = 0; i<list.size(); i++) {
			result.enqueue(list.get(i));
		}
		list.clear();
		return (Queue<String>) result;
		
	}

	public Stack<K> keyStack(SCDLL_Map map){ // Returns a stack of all keys in the Map in "First In-Last Out Format". Complexity O(n).
		Stack<K> stack = new Stack<K>();
		EList<K> temp = map.getKeys();
		for(int i=0; i<temp.size(); i++) { //O(n)
			stack.push(temp.get(i)); //O(1)
		}
		temp.clear();
		return stack;
	}


}



